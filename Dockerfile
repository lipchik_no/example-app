FROM python:alpine

RUN pip install flask redis

COPY src /src/

EXPOSE 8000

CMD ["python", "/src/app.py"]
