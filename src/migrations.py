import redis
import os

redis_host = os.environ['REDIS_HOST']
redis_port = os.environ['REDIS_PORT']

r = redis.Redis(host=redis_host, port=redis_port)
r.set('foo', 'bar')
