from flask import Flask, request, abort
import os
import redis

redis_host = os.environ['REDIS_HOST']
redis_port = os.environ['REDIS_PORT']

app = Flask(__name__)


@app.route('/', methods=['GET'])
def myapp():
    return "<h1>Hello World!</h1>"


@app.route('/alive', methods=['GET'])
def alive():
    try:
        r = redis.Redis(host=redis_host, port=redis_port)
        r.ping()
        return "<h1>Alive!</h1>"
    except redis.RedisError:
        abort(503)


@app.route('/ready', methods=['GET'])
def ready():
    try:
        r = redis.Redis(host=redis_host, port=redis_port)
        r.ping()
        return "<h1>Ready!</h1>"
    except redis.RedisError:
        abort(503)

@app.route('/metrics', methods=['GET'])
def metrics():
    return '''metric_foo 1
metric_bar 2'''

if __name__ == '__main__':
    app.run(debug=False, port=8000, host='0.0.0.0')