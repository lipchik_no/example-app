#!/usr/bin/env bash

APP_VERSION=$1

helm package --app-version=release-$APP_VERSION app-chart

docker build -t navyzet/example-app:$APP_VERSION .

docker push navyzet/example-app:$APP_VERSION

helm upgrade -n default --install example-app app-chart-2.2.0.tgz --wait --atomic --set appVersion=$APP_VERSION --values=.cicd/deploy/helm-envs/common.yaml

