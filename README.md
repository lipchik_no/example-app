## Установка Helm
https://helm.sh/ru/docs/intro/install/

    brew install helm 

### Подключение helm репозитория

    helm repo add bitnami https://charts.bitnami.com/bitnami
    helm repo update

### Локальный кластер

    kind create cluster --name local
    kubectl get nodes

### Установка чарта

    helm search repo redis
    helm show chart bitnami/redis

    helm install my-redis  bitnami/redis

### Проверка работоспособности

    export REDIS_PASSWORD=$(kubectl get secret --namespace default my-redis -o jsonpath="{.data.redis-password}" | base64 --decode)
    echo $REDIS_PASSWORD
    kubectl run --rm -it redis-client --restart='Never'  --env REDISCLI_AUTH=$REDIS_PASSWORD  --image docker.io/bitnami/redis:6.2.6-debian-10-r49 --command -- bash
    redis-cli -h my-redis-master
    ping
    SET mykey "Hello"
    GET mykey
    helm uninstall my-redis
    
### Подготовка стэнда

    kubectl create namespace monitoring
    helm repo add prometheus-community https://prometheus-community.github.io/helm-charts
    helm repo update
    helm install -n monitoring prometheus-community prometheus-community/kube-prometheus-stack --version 19.1.0 --values ./prometheus.yaml

### Создание тестового приложения

### Сборка приложения

    docker login
    docker build -t navyzet/example-app:0.0.1 .
    docker push navyzet/example-app:0.0.1

### Подготовка чарта

    git clone --branch 2.2.0 git@gitlab.com:magnit-online-services/app-loyalty/infra/app-chart.git
    helm package --app-version=release-0.0.1 app-chart

### Установка приложения

    helm upgrade -n default -i example-app app-chart-2.2.0.tgz --wait --atomic --values=.cicd/deploy/helm-envs/common.yaml
    helm upgrade -n default --install example-app app-chart-2.2.0.tgz --wait --atomic --set appVersion=0.0.1 --values=.cicd/deploy/helm-envs/common.yaml

### Проверка приложения

    kubectl port-forward service/example-app 8080:80
    kubectl -n monitoring port-forward service/prometheus-community-kube-prometheus 9090:9090

### Установка Redis

    ./ci.sh 0.0.2
